#!python3
# -*- coding: latin-1 -*-
import webbrowser, sys, pyperclip, bs4, requests, re, sqlite3
from nltk.tokenize import word_tokenize

#TODO: tokenize, save in database 
def start():
   if len(sys.argv) > 1:
      adress = 'http://' + sys.argv[1]
   else:
      adress = 'http://' + pyperclip.paste()
   

   get_page = requests.get(adress)
   get_page.raise_for_status()
   soup = bs4.BeautifulSoup(get_page.text, "html.parser")
   web_adress = soup.find_all('span')
   counter = 0
   for tag in web_adress:
            
      print("[%s] -> %s" %(counter, tag.text))
      counter+= 1
   
   word_toke(web_adress[367].text)

def word_toke(sent):
   sent_list = word_tokenize(sent)
   for word in sent_list:
      print(word)

start()
